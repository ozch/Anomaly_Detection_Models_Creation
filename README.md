# Anomaly_Detection_Models_Creation
<h2> if you're here. you know what you want. So you can figure it out your self :) </h2>

# Dataset
  <p>NSL KDD Intrusion Detection Dataset <emp>(it's kddcup99 on steroids)</emp></p>
  
# Papers:
<pre>
     1. Martina Troesch, Ian Walsh, Machine Learning for Network Intrusion Detection.pdf
     2. Network Intrusion Detection Using Machine Learning .pdf
     3. Toward Generating New ID Dataset and Intrusion Trafﬁc .pdf
     4. NSL-KDD99 (Doc from the some offical website)
     5. A Study on NSL KDD Dataset for IDS.pdf
     6. A Detailed Analysis of the KDD CUP 99 Data Set.pdf
     7. Feature Extraction Mehtod for IDS.pdf  
</pre>
